# Zaurbox

## What is Zaurbox?

Zaurbox is a simple implementation of UNIX standard utilities.
At the moment in Zaurubox there are cat, ls, sh, sleep, texor, 
wc and yes.

## License

Zaurbox is licensed under the GNU GPL v3-or-later with some components 
under different, but compatible licenses.