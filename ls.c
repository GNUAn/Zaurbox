#include <dirent.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

/*
        ls for Zaurbox
        Copyright (C) 2024  AnatoliyL

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

void ls(char*, int, int);
void ls(path, a, b)
char *path;
int a;
int b;
{
  struct dirent *d;
  DIR *dh = opendir(path);
  if (!dh)
    {
      perror("Directory doesn't exist\n");
    }
  else
    {
      while ((d = readdir(dh)) != NULL)
        {
                if (!a && d->d_name[0] == '.')
                        continue;
                  printf("%s  ", d->d_name);
                if(b) printf("\n");
        }
    }
}
int main(argc, argv)
int argc;
char **argv;
{
  char *arg1 = argv[1];
  if (argc < 2)
    {
      ls(".", 0, 0);
    }
  else
    ls(arg1, 0, 0);
  putchar('\n');
  return 0;
}
