#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define x *

/*
        Calculator for Zaurbox
        Copyright (C) 2024  AnatoliyL

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


long long int exponent(long long int a, long long int b) // Function for exponentiation
{
    long long int a2 = a;
    if (b == 0)
        return 1;
    else
    {
        for (int i = 1; i < b; i++)
        {
            a*=a2;
        }
    }
    return a;
}
int main(int argc, char **argv)
{
    long long int a, b;
    if (argc < 4) // If no arguments specified
    {
        printf("No arguments specified!\n");
        return -1; // End of program with error
    }
    a = atoi(argv[1]);
    b = atoi(argv[3]);
    int c = argv[2][0];
    if (c == '+')
    {
        printf("%lld\n", a + b);
    }
    else if (c == '-')
    {
        printf("%lld\n", a - b);
    }
    else if (c == '*' || c == 'x')
    {
        printf("%lld\n", a x b);
    }
    else if (c == '/')
    {
        if (b == 0)
        {
            printf("You cannot divide by zero!\n");
            return -1;
        }
        else
            printf("%lld\n", a / b);
    }
    else if (c == '^')
    {
        printf("%lld\n", exponent(a, b));
    }
    return 0;
}