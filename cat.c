#include <stdio.h>
#include <string.h>

/*
        Cat for Zaurbox
        Copyright (C) 2024  AnatoliyL

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#define failure -1
#define success 0
#define both_failed -2

void printfile(FILE *f)
{
  int c;
  while ((c = getc(f)) != EOF)
    {
      putchar(c);
    }
}

int main(int argc, char **argv)
{
  if (argc < 4)
  {
    int c;
    FILE *f;
    f = fopen(argv[1], "r");
    if (!f && strcmp(argv[1], "") != 0)
      {
        puts("Error! No such file!");
        return -1;
      }
    else if (!f && strcmp(argv[1], "") == 0)
      {
	      puts("You haven't specified a filename!");
	      return -1;
      }
    printfile(f);
    fclose(f);
    c = 0;
    putchar('\n');
  }
  else if (argv[2][0] == '>' && argc == 4)
  {
    int c;
    FILE *f;
    f = fopen(argv[1], "r");
    FILE *f2 = fopen(argv[2], "w");
    if (!f)
      {
        printf("Error! No such file!\n");
        fclose(f);
        return failure;
      }
      if (!f2)
      {
        printf("Error! No such file!\n");
        fclose(f);
        return failure;
      }
     if (!f && !f2)
      {
        printf("Error! Both files doesn't exist!\n");
        fclose(f);
        fclose(f2);
        return both_failed;
      }
    while ((c = getc(f)) != EOF)
      {
        if (f2 != fopen("/dev/null", "w"))
          putc(c, f2);
      }
    fclose(f);
    fclose(f2);
    c = 0;
    putchar('\n');
  }
  else
  {
    printf("You forgot to put '>' before the file name.\n");
    return failure;
  }
  return success;
}
